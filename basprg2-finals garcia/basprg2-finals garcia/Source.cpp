#include "Pokemon.h"
#include <string>
#include <iostream>
#include <vector>
#include <time.h>

using namespace std;

void Intro(string& TrainerName, int& choice) {

	cout << "Hi Trainer! What is your name?" << endl;
	cin >> TrainerName;

	cout << "I have three pokemons here. Go on, choose one" << endl;
	cout << endl;
	cout << "1 - Bulbasaur		Grass type		level 5" << endl;
	cout << "2 - Charmander		Fire type		level 5" << endl;
	cout << "3 - Squirtle		Water type		level 5" << endl;
	cout << endl;
	cout << "input choice: ";
	cin >> choice;

}

void walk(int& x, int& y) {
	system("cls");
	string choice;
	cout << "Where do you want to move?" << endl;
	cout << "[w] - Up		[s] - Down		[a] - Left		[d] - Right" << endl;
	cin >> choice;

	if (choice == "w") {
		y++;
	}
	if (choice == "s") {
		y--;
	}
	if (choice == "a") {
		x--;
	}
	if (choice == "d") {
		x++;
	}

	cout << "you are now at position <" << x << ", " << y << ">" << endl;

}

void Whattodo(bool safezone) {
	cout << "What would you like to do?" << endl;
	cout << "[1] Walk " << endl;
	cout << endl;
	cout << "[2] Pokemons" << endl;
	cout << endl;
	if (safezone == true) {

		cout << "[3] Pokecenter" << endl;

	}

}

void battle(Pokemon* enemy) {

	cout << endl;
	enemy->pokemonDisplay();

	cout << "What would you like to do ?" << endl;
	cout << "[a] Attack        [b] Flee			[c] Catch     " << endl;

}

Pokemon* allPokemon(int pokedex) {

	int levelRandom = rand() % 10 + 1;

	switch (pokedex)
	{
	case 1:return new Pokemon("Pikachu", "Electric", levelRandom, 35, 55, 0, 20);
	case 2:return new Pokemon("Ratata", "Earth", levelRandom, 30, 56, 0, 20);
	case 3:return new Pokemon("Bulbasaur", "Grass", levelRandom, 45, 49, 0, 20);
	case 4:return new Pokemon("Charmander", "Fire", levelRandom, 39, 52, 0, 20);
	case 5:return new Pokemon("Squirtle", "Water", levelRandom, 44, 48, 0, 20);
	case 6:return new Pokemon("Caterpie", "Bug", levelRandom, 45, 30, 0, 20);
	case 7:return new Pokemon("Weedle", "Bug", levelRandom, 40, 35, 0, 20);
	case 8:return new Pokemon("Pidgey", "Normal", levelRandom, 40, 45, 0, 20);
	case 9:return new Pokemon("Ekans", "Poison", levelRandom, 35, 60, 0, 20);
	case 10:return new Pokemon("Sandshrew", "Ground", levelRandom, 50, 75, 0, 20);
	case 11:return new Pokemon("Zubat", "Poison", levelRandom, 40, 45, 0, 20);
	case 12:return new Pokemon("Diglett", "Ground", levelRandom, 10, 55, 0, 20);
	case 13:return new Pokemon("Meowth", "Normal", levelRandom, 40, 45, 0, 20);
	case 14:return new Pokemon("Abra", "Psychic", levelRandom, 25, 20, 0, 20);
	case 15:return new Pokemon("Machop", "Fighting", levelRandom, 70, 80, 0, 20);

	}
}

void wildPusinIn(vector<Pokemon*> &wildpokemon) {

	wildpokemon.clear();

	for (int i = 0; i <= 15; i++) {

		wildpokemon.push_back(allPokemon(i));

	}
}

void changePokemon(vector<Pokemon*> inventory, int& currentpokemon) {

	for (int i = 0; i < inventory.size(); i++) {

		if (inventory[i]->isdead()) {

			currentpokemon++;

		}
	}
}

void healPokemon(vector<Pokemon*> inventory) {

	cout << "your pokemon are now healed" << endl;
	for (int i = 0; i < inventory.size(); i++) {

		inventory[i]->heal();

	}

}

int main() {

	vector<Pokemon*> inventory;
	vector<Pokemon*> wildPokemon;
	string TrainerName;
	int choice = 0;
	int x = 0;
	int y = 0;

	bool safezone = true;
	srand(time(NULL));

	Intro(TrainerName, choice);

	if (choice == 1) {

		Pokemon* bulbasaur = new Pokemon("Bulbasaur", "Grass", 5, 45, 49, 0, 20);
		inventory.push_back(bulbasaur);

	}
	else if (choice == 2) {

		Pokemon* charmander = new Pokemon("Charmander", "Fire", 5, 39, 52, 0, 20);
		inventory.push_back(charmander);

	}
	else if (choice == 3) {

		Pokemon* squirtle = new Pokemon("Squirtle", "Water", 5, 44, 48, 0, 20);
		inventory.push_back(squirtle);

	}
	cout << "Your journey begins now" << endl;
	system("cls");
	while (true) {
		choice = 0;

		Whattodo(safezone);

		cin >> choice;
		system("cls");

		if (choice == 1) {

			walk(x, y);
			if (x <= 2 && y <= 2 && x >= -2 && y >= -2) {
				cout << "you are in Pallet Town" << endl;
				safezone = true;
			}
			if (y <= 10 && y >= 8) {
				cout << "you are in Vermilion City" << endl;
				safezone = true;
			}
			else if (x >= 3 || y >= 3 || x <= -3 || y <= -3)
			{

				int randomEncounter = rand() & 100;
				cout << "you are in the wild" << endl;
				if (y >= 3 && y <= 7) {
					cout << "||Route 1||" << endl;
				}

				safezone = false;
				if (randomEncounter <= 50) {

					wildPusinIn(wildPokemon);
					int wildpokemonRandom = rand() % wildPokemon.size();

					string Battlechoice;

					cout << "Wild pokemon has appeared! " << endl;
					int currentpokemon = 0;
					changePokemon(inventory, currentpokemon);

					while (true) {

						battle(wildPokemon[wildpokemonRandom]);
						cin >> Battlechoice;
						system("cls");

						if (Battlechoice == "a") {

							inventory[currentpokemon]->attack(wildPokemon[wildpokemonRandom]);

							if (wildPokemon[wildpokemonRandom]->isdead()) {
								break;
							}

							wildPokemon[wildpokemonRandom]->attack(inventory[currentpokemon]);

							if (inventory[currentpokemon]->isdead())
							{
								changePokemon(inventory, currentpokemon);
							}
							if (currentpokemon >= inventory.size())
							{
								cout << "all of your pokemon fainted" << endl;
								break;
							}
						}

						if (Battlechoice == "c") {
							int catchChance = rand() % 100;
							cout << "You try to catch the pokemon..." << endl;
							system("pause");
							if (catchChance <= 30) {

								cout << "congratulation! you caught new pokemon!" << endl;
								inventory.push_back(wildPokemon[wildpokemonRandom]);
								break;

							}
							else {

								cout << "you failed to catch the pokemon" << endl;

							}
						}
					}
				}
			}
		}
		if (choice == 2) {

			for (int i = 0; i < inventory.size(); i++) {

				inventory[i]->pokemonDisplay();

			}

		}

		if (choice == 3 && safezone == true) {

			healPokemon(inventory);

		}

		system("pause");
		system("cls");

	}
	system("pause");
}