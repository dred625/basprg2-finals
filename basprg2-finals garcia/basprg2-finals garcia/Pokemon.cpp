#include "Pokemon.h"
#include <string>
#include <iostream>

using namespace std;

Pokemon::Pokemon()
{
	name = " ";
	type = " ";
	level = 0;
	hp = 30;
	damage = 0;
	exp = 0;
}

Pokemon::Pokemon(string name, string type, int level, int baseHp, int damage, int exp, int expToNextLevel)
{
	this->name = name;
	this->type = type;
	this->level = level;
	this->baseHp = baseHp;
	this->hp = ((baseHp * .15) * level) + baseHp;
	this->damage = ((damage * .10) * level) + damage;
	this->exp = exp;
	this->expToNextLevel = ((expToNextLevel * .20)*level) + expToNextLevel;
}


void Pokemon::pokemonDisplay()
{
	cout << this->name << " stats: " << endl;
	cout << "Type: " << this->type << endl;
	cout << "Level: " << this->level << endl;
	cout << "HP: " << this->hp << endl;
	cout << "Damage: " << this->damage << endl;
	cout << "Exp: " << this->exp << "/ " << ((this->expToNextLevel * .20) *  this->level) + this->expToNextLevel << endl;
	cout << endl;
}

void Pokemon::attack(Pokemon* enemy)
{
	int hitChance = rand() % 100;


	if (hitChance <= 80) {

		cout << this->name << " attacked " << enemy->name << endl;
		cout << enemy->name << " took " << this->damage << " damage" << endl;
		enemy->hp -= this->damage;
		cout << enemy->name << " has " << enemy->hp << " hp left" << endl;


		if (enemy->hp <= 0) {
			cout << enemy->name << " is dead" << endl;
			cout << this->name << " earned " << (this->expToNextLevel * .2) + this->exp << " exp" << endl;
			this->exp = (this->expToNextLevel * .2) + this->exp;
			if (this->exp >= this->expToNextLevel) {

				this->level++;
				cout << this->name << " leveled up!" << endl;

			}
		}
	}
	else {

		cout << this->name << " missed" << endl;
		cout << enemy->name << " has " << enemy->hp << " hp left" << endl;
		cout << endl;

	}

	cout << endl;
}

void Pokemon::heal()
{

	this->hp = ((this->baseHp * .15) * this->level) + this->baseHp;

}

bool Pokemon::isdead()
{
	return hp <= 0;
}
