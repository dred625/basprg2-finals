#pragma once

#include <string>

using namespace std;


class Pokemon {
public:
	Pokemon();
	Pokemon(string name, string type, int level, int baseHp, int damage, int exp, int expToNextLevel);
	void pokemonDisplay();
	void attack(Pokemon* enemy);
	void heal();
	bool isdead();

private:

	string name;
	string type;
	int level;
	int baseHp;
	int hp;
	int damage;
	int exp;
	int expToNextLevel;
};